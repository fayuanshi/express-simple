const mongoose = require('mongoose')
mongoose.connect('mongodb://admin:shifayuan@121.36.90.111:27017/express',{
    useNewUrlParser: true,
    useUnifiedTopology: true,
    authSource:"admin",   //
    useCreateIndex:true
}) 

const UserSchema = {
    username:{type:String,unique:true},  //unique 限定字段值唯一
    password:{
        type:String,
        set(val){
            return require('bcrypt').hashSync(val,10)
            
        }
    }
}

const User = mongoose.model('User',UserSchema,"User")


module.exports = {
    User

}